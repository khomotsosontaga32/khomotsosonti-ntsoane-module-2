import 'dart:io';

void main() {
  appMy appWinner = appMy();
  appWinner.nameOfApp = "Ambani Africa";
  appWinner.nameOfDeveloper = "Mukundi Lambani";
  appWinner.category = "Language and Teaching";
  appWinner.yearItWon = 2021;
  //print output
  appWinner.display();
  print(appWinner.nameOfApp);
}

class appMy {
  String? nameOfApp, nameOfDeveloper, category;
  int? yearItWon;
  void display() {
    print(
        "The name of the winning app is $nameOfApp is a $category type of app and the year it won the MTN award is $yearItWon by a man called $nameOfDeveloper");
  }

  void toUpperCase(var line) {
    print(line.toUpperCase());
  }
}
